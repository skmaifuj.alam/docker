from flask import Flask,request
from flask_restplus import Api, swagger, Resource, Namespace
import flask_restplus
import json
from Database.CURDG import CURDG
app = Flask(__name__)
api = Api(app, version='1.3', title="New Title", description="Its a Hello World App", contact_email='abc@gmail.com')
#NS = Namespace(name="Sample ", description="Description 1")
model1 = api.model("Users", {
    'id': flask_restplus.fields.Integer(required=True, description="Id of the user"),
    'name': flask_restplus.fields.String(required=True, description="Name of User"),
    'age':flask_restplus.fields.Integer(required=False,description="Stores the Age of User")
})
model2 = api.model("List Users", {
    'list_users': flask_restplus.fields.List(flask_restplus.fields.Nested(model1), description="List of Users")
})
SIMPLE_RESPONSE = api.model('simple_response',
    {
         'id': flask_restplus.fields.Integer(required=True, description="Id of the user"),
         'name': flask_restplus.fields.String(required=True, description = "Name of the user"),
         'age':flask_restplus.fields.Integer(required=False,description="Stores the Age of User")
    }
)
@api.route('/create/')
class Language(Resource):
    @api.expect(SIMPLE_RESPONSE, validate=True)  #?
    def post(self):                       # Create
        #data=request.get_json()
        ob = CURDG()  # Creating Object of CURDG Class
        ob.create(request.get_json())
        return "Inserted",201

@api.route('/update/<string:olds>/<string:news>')
class s2(flask_restplus.Resource):
    def put(self, olds,news):
        ob = CURDG()  # Creating Object of CURDG Class
        data1 = ob.update(olds,news)  #
        return "Updated",201


@api.route('/read_all/')
class get_all(Resource):
    def get(self):
        ob = CURDG()  # Creating Object of CURDG Class
        data1 = ob.readAll()  # Calling method and storing its cursor object to data
        xx = list(data1)  # Parsing object cursor to list of dictionary
        return xx,200

@api.route('/read/<int:id>')     #Hare it accpet it as a parameter
class S1(flask_restplus.Resource):
    def get(self,id):
        ob=CURDG()
        result=ob.read(id)
        result_list=list(result)
        #print(result_list)
        return result_list

@api.route('/delete/<int:id>')     #Hare it accpet it as a parameter
class S1(flask_restplus.Resource):
    def delete(self,id):
        ob = CURDG()
        result = ob.delete(id)
        gg=(str)(result)+" items deleted"

        return gg,200

if __name__ == '__main__':
    app.run(debug=True,port=5000,host='0.0.0.0')
